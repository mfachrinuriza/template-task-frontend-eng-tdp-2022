import axios from 'axios';
import React, { useEffect, useState } from 'react'

const MyProfile = () => {
    const [isLoading, setisLoading] = useState(false);
    const [isError, setisError] = useState(false);
    
    const [profile, setProfile] = useState({
        name: "",
        email: "",
        phoneNumber: ""
    })

    const [password, setPassword] = useState({
        password: "",
        password2: ""
    })

    const [notif, setNotif] = useState({
        showMessage: false,
        field: '',
        messageContent: ''
    });

    //get api myprofile
    const username = window.localStorage.getItem("username");
    const getProfile = async () =>{
        try{
            let respons = await axios.get(`https://api-tdp-2022.vercel.app/api/profile/${username}`);
            setProfile(respons.data.data);
            setisLoading(false);
            }
        catch(e){
            setisError(true);
        }
    }

    useEffect(()=>{
        setisLoading(true);
        getProfile();
    }, [])

    const handleChange = (e) => {
        let data = {...profile};
        data[e.target.name] = e.target.value;
        setProfile(data)

        let pass = {...password};
        pass[e.target.name] = e.target.value;
        setPassword(pass)
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        let data = {...profile};
        
        if(profile.name === ""){
            setNotif({
                showMessage: true,
                field: 'name',
                messageContent: "Name is required"
            })
        }else if(profile.email === ""){
            setNotif({
                showMessage: true,
                field: 'email',
                messageContent: "Email is required"
            })
        }else if(profile.phoneNumber === ""){
            setNotif({
                showMessage: true,
                field: 'phoneNumber',
                messageContent: "Phone number is required"
            })
        }else if(profile.password === ""){
            setNotif({
                showMessage: true,
                field: 'password',
                messageContent: "Password is required"
            })
        }else if(profile.password2 === ""){
            setNotif({
                showMessage: true,
                field: 'password2',
                messageContent: "Retype Password is required"
            })
        }else if(profile.username !== profile.password2){
            setNotif({
                showMessage: true,
                field: 'password2',
                messageContent: "Retype Password invalid"
            })
        }else{
            const update = {
                name: profile.name,
                email: profile.email,
                phoneNumber: profile.phoneNumber,
                password: password.password2
            }
            let value = axios.put( `https://api-tdp-2022.vercel.app/api/profile/${username}`, update)
            .then(result => {
            if(result){
                // console.log(result?.data?.message)
                alert(result?.data?.message)
                window.location.replace("/");
            }
            })
        }
    }

    

  return (
    <div className="content padding" style={{ maxWidth: '1564px' }}>
        <div className="container padding-32" id="contact" >
            <h3 className="border-bottom border-light-grey padding-16">My Profile</h3>
            <p>Lets get in touch and talk about your next project.</p>
            <form onSubmit={handleSubmit}>
                <input className="input section border" type="text" placeholder="Name" onChange={handleChange} value={profile.name} name="name" />
                {
                    (notif.showMessage && (notif.field==="name"))?
                    <div style={{ color: '#EF144A' }}>{notif.messageContent}</div>
                    :''
                }
                <input className="input section border" type="text" placeholder="Email" onChange={handleChange} value={profile.email} name="email" />
                {
                    (notif.showMessage && (notif.field==="email"))?
                    <div style={{ color: '#EF144A' }}>{notif.messageContent}</div>
                    :''
                }
                <input className="input section border" type="text" placeholder="Phone Number" onChange={handleChange} value={profile.phoneNumber} name="phoneNumber" maxLength={12} />
                {
                    (notif.showMessage && (notif.field==="phoneNumber"))?
                    <div style={{ color: '#EF144A' }}>{notif.messageContent}</div>
                    :''
                }
                <input className="input section border" type="password" placeholder="Password" name="password" onChange={handleChange} value={password.password} />
                {
                    (notif.showMessage && (notif.field==="password"))?
                    <div style={{ color: '#EF144A' }}>{notif.messageContent}</div>
                    :''
                }
                <input className="input section border" type="password" placeholder="Retype Password" name="password2" onChange={handleChange} value={password.password2} />
                {
                    (notif.showMessage && (notif.field==="password2"))?
                    <div style={{ color: '#EF144A' }}>{notif.messageContent}</div>
                    :''
                }
                <button className="button black section" type="submit">
                    <i className="fa fa-paper-plane" /> Update
                </button>
            </form>
        </div>
    </div>
  )
}

export default MyProfile