import Header from 'components/Header'
import ProductCategory from 'components/ProductCategory'
import React from 'react'

const Home = () => {
    return (
        <>
            <Header/>
            <ProductCategory/>
        </>
    )
}

export default Home