import axios from 'axios';
import React, { useState } from 'react'

const Login = () => {
    //state formData
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    
    const onChangeUsername = (e) =>{
        const value = e.target.value
        setUsername(value);
    }

    const onChangePassword = (e) =>{
        const value = e.target.value
        setPassword(value);
    }

    const submitLogin = (e) => {
        e.preventDefault();
        
        //format hit api
        const data = {
            username: username,
            password: password
        }

        if(username === "" || password === ""){
            alert("Username or password is required")
        }

        axios.post(`https://api-tdp-2022.vercel.app/api/login`, data)
        .then(result => {
            if(result){
                window.localStorage.setItem('token', result?.data?.data?.access_token)
                window.localStorage.setItem('username', username)
                window.location.replace("/")
            }
        })
        .catch(e => {
            alert(e?.response?.data?.message)
        })
    }

    return (
        <div className="content padding" style={{ maxWidth: '1564px' }}>
            <div className="container padding-32" id="contact" >
                <h3 className="border-bottom border-light-grey padding-16">Login</h3>
                <p>Lets get in touch and talk about your next project.</p>
                <form>
                    <input className="input border"  onChange={onChangeUsername} type="text" placeholder="Username" required name="username" />
                    <input className="input section border" onChange={onChangePassword} type="password" placeholder="Password" required name="Password" />
                    <button className="button black section" type="submit" onClick={submitLogin}>
                        <i className="fa fa-paper-plane" />
                        Login
                    </button>
                </form>
            </div >
        </div>
    )
}

export default Login