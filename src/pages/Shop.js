import AllProduct from 'components/AllProduct'
import ProductCategory from 'components/ProductCategory';
import ProductDetail from 'components/ProductDetail';
import React, { useState } from 'react'
import { useLocation, useParams } from 'react-router-dom';

const Shop = () => {
    const {productId} = useParams();
    console.log(productId);

    // const {categoryId} = useParams();
    function useQuery() {   
        return new URLSearchParams(useLocation().search);
    }
    let category = useQuery();
    const categoryId = category.get("category")
    
    console.log(categoryId)
    // function handleCategoryID(){
    // }
    

  return (
    <div>
        {   
            categoryId ?
            <AllProduct categoryId={categoryId}/>
            :
            productId ?
            <ProductDetail/> 
            :
            !categoryId ?
            <AllProduct categoryId={categoryId}/>
            :
            <ProductCategory />
        }
    </div>
  )
}

export default Shop