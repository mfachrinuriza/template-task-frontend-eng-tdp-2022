import 'assets/css/style.css';
import 'assets/css/custom.css';
import Navbar from 'components/Navbar';
import { Route, Routes } from 'react-router-dom';
import Home from 'pages/Home';
import Footer from 'components/Footer';
import Shop from 'pages/Shop';
import Login from 'pages/Login';
import MyProfile from 'pages/MyProfile';

const App = () => {
    return (
        <>
            <Navbar/>
            <Routes>
                <Route path='/' element={<Home/>} />
                <Route path='/login' element={<Login/>} />
                <Route path='/profile' element={<MyProfile/>} />

                <Route path='/shop' element={<Shop/>} />
                <Route path='/shop/:productId' element={<Shop/>} />
                <Route path='/shop?category=:categoryId' element={<Shop/>} />

            </Routes>
            <Footer/>
        </>
    );
}

export default App;
