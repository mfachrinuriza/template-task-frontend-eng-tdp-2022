import React from 'react'
import map from 'assets/img/map.jpg';

const Map = () => {
  return (
    <div>
        <div className="container padding-32">
            <img src={map} className="image" alt="maps" style={{ width: '100%' }} />
        </div>
    </div>
  )
}

export default Map