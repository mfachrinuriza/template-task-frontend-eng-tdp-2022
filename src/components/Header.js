import banner from 'assets/img/architect.jpg';
import React from 'react'

const Header = () => {
  return (
    <div>
        <header className="display-container content wide" style={{ maxWidth: '1500px' }} id="home">
            <img className="image" src={banner} alt="Architecture" width={1500} height={800} />
            <div className="display-middle center">
                <h1 className="xxlarge text-white">
                    <span className="padding black opacity-min">
                        <b>EDTS</b>
                    </span>
                    <span className="hide-small text-light-grey">Mart</span>
                </h1>
            </div>
        </header>
    </div>
  )
}

export default Header