import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';

const AllProduct = ({categoryId}) => {
    const [products, setProducts] = useState([]);
    const [isLoading, setisLoading] = useState(false);
    const [isError, setisError] = useState(false);

    const getProducts = async () =>{
        try{
            if(categoryId){
                let respons = await axios.get(`https://api-tdp-2022.vercel.app/api/products?category=${categoryId}`);
                setProducts(respons.data.data.productList);
                setisLoading(false);
            }else{
                let respons = await axios.get('https://api-tdp-2022.vercel.app/api/products');
                setProducts(respons.data.data.productList);
                setisLoading(false);
            }
        }catch(e){
            setisError(true);
            setisLoading(false);
        }
    }
    
    useEffect(()=>{
        setisLoading(true);
        getProducts();
    }, [])
    
  return (
    <div>
        <div className="content padding" style={{ maxWidth: '1564px' }}>
            <div className="container padding-32" id="about">
                <h3 className="border-bottom border-light-grey padding-16">
                    {
                        !categoryId?
                        'All Product'
                        : `Product Category ${products[0]?.category}`
                    }
                </h3>
            </div>
            <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
            {
            !isLoading ? products.map((product, index) => {
            return(
                    <div className="product-card" key={index}>
                        {
                            product.discount > 0 ?
                            <div className="badge">Discount</div>:''
                        }
                        <div className="product-tumb">
                            <img src={product.image} alt="product1" />
                        </div>
                        <div className="product-details">
                            <small className='float right' style={{color:'lightgrey'}}>Stock : {product.stock}</small>
                            <span className="product-catagory">{product.category}</span>
                            <h4>
                                <a href={`/shop/${product.id}`}>{product.title}</a>
                            </h4>
                            <p>{product.description}</p>
                            <div className="product-bottom-details">
                                <div className="product-price">Rp. {product.price}</div>
                                <div className="product-links">
                                    <Link to={`/shop/${product.id}`}>View Detail<i className="fa fa-heart"></i></Link>
                                </div>
                            </div>
                        </div>
                    </div>
            )
            }):''
        }
            </div>
            {
                isLoading ? 
            <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
                Loading . . .
            </div>
            : isError ? 
            <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
                Something error
            </div>
            :''    
            }   
        </div>
    </div>
  )
}

export default AllProduct