import React from 'react'

const Navbar = () => {
    const isLoggedIn = window.localStorage.getItem("token");

    const handleLogout = () => {
        localStorage.clear()
        window.location.reload()
    }
  return (
    <div>
        <div className="top" >
            <div className="bar white wide padding card">
                <a href="/" className="bar-item button"><b>EDTS</b> TDP Batch #2</a>
                <div className="right hide-small">
                    <a href="/" className="bar-item button">Home</a>
                    <a href="/shop" className="bar-item button">Shop</a>
                    {/* <a href="/cart" className="bar-item button">My Cart</a> */}
                    <a href="/profile" className="bar-item button">My Profile</a>
                    {
                        isLoggedIn?
                        <a href="/" style={{ cursor: 'pointer' }} className="bar-item button" onClick={handleLogout}>Logout</a>
                        :
                        <a href="/login" className="bar-item button">Login</a>
                    }
                </div>
            </div>
        </div>
    </div>
  )
}

export default Navbar