import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import TitlePage from './TitlePage';

const ProductDetail = () => {
    const isLoggedIn = window.localStorage.getItem("token");
    
    const {productId} = useParams();
    const [detailProduct, setDetailProduct] = useState([]);
    const [isLoading, setisLoading] = useState(false);
    const [isError, setisError] = useState(false);
    
    const [quantity, setQuantity] = useState({})
    
    const handleSubmit = (e) =>{
        e.preventDefault();
        
        if(quantity === 0 || quantity === "" || !quantity){
            alert('You must insert quantity')
            return false;
        }else if(isLoggedIn){
            alert('Product added to cart successfully')
            // window.location.replace("/");
        }else if(!isLoggedIn){
            alert('Please Login First')
            window.location.replace("/login");
        }else{
            alert('Something error...')
            // window.location.replace("/login");
        }
    }

    function currencyFormat(num) {
        return 'Rp ' + num.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }

    let QuantityInt = parseInt(quantity);
    let num = detailProduct.price;
    let diskon = 1 - (detailProduct?.discount / 100);
    let totalHarga = currencyFormat(num * QuantityInt);
    let hargaDiskon = currencyFormat(num * QuantityInt * diskon)
    let dataPrice = parseInt(detailProduct.price)
    let price = currencyFormat(dataPrice)
    

    // console.log(productId)
    const getDetailProduct = async ()=>{
        try{
            let respons = await axios.get(`https://api-tdp-2022.vercel.app/api/products/${productId}`)
            setDetailProduct (respons.data.data)
            setisLoading(false);
        }catch(e){
            setisError(true);
        }
    }
    
    useEffect(()=>{
        setisLoading(true);
        getDetailProduct()
    }, [productId])


    return (
    <div className="content padding" style={{ maxWidth: '1564px' }}>
        <TitlePage/>
        {
            !isLoading ?
            <div className="row-padding card card-shadow padding-large" style={{ marginTop: '50px' }}>
                <div className="col l3 m6 margin-bottom">
                    <div className="product-tumb">
                        <img src={detailProduct.image} alt="Product 1" />
                    </div>
                </div>
                <div className="col m6 margin-bottom">
                    <h3>{detailProduct.title}</h3>
                    <div style={{ marginBottom: '32px' }}>
                        <span>Category : <strong>{detailProduct.category}</strong></span>
                        <span style={{ marginLeft: '30px' }}>Review : <strong>{detailProduct.rate}</strong></span>
                        <span style={{ marginLeft: '30px' }}>Stock : <strong>{detailProduct.stock}</strong></span>
                        <span style={{ marginLeft: '30px' }}>Discount : <strong>{detailProduct.discount} %</strong></span>
                    </div>
                    <div style={{ fontSize: '2rem', lineHeight: '34px', fontWeight: '800', marginBottom: '32px' }}>
                        Rp. {detailProduct.price}
                    </div>
                    <div style={{ marginBottom: '32px' }}>
                        {detailProduct.description}.
                    </div>
                    <div style={{ marginBottom: '32px' }}>
                        <div><strong>Quantity : </strong></div>
                        {
                            detailProduct.stock >0? 
                            <input type="number" id='quantity' className="input section border" name="total"  min="0" max={detailProduct.stock} onChange={event => setQuantity(event.target.value)} defaultValue='0' required />
                            :''
                        }
                    </div>
                    <div style={{ marginBottom: '32px', fontSize: '2rem', fontWeight: 800 }}>
                        Sub Total : Rp. 
                        {hargaDiskon}
                        
                        {
                            detailProduct.discount > 0 ?
                            <span style={{ marginLeft: '30px', fontSize: '18px', textDecoration: 'line-through' }}><strong>Rp. {totalHarga}</strong></span>:''
                        }
                    </div>
                    {
                        detailProduct.stock > 0 ?
                        <button className='button light-grey block' onClick={handleSubmit}>Add to cart</button>
                        :
                        <button className='button light-grey block' disabled={true}>Empty Stock</button>
                    }
                </div>
            </div>:
            <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
                Loading . . .
            </div>
        }
    </div>
    )
}

export default ProductDetail