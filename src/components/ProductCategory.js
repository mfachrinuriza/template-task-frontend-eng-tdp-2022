import axios from 'axios';
import React, { useEffect, useState } from 'react'
import Map from './Map'

const ProductCategory = () => {
    const [products, setProducts] = useState([]);
    const [isLoading, setisLoading] = useState(false);
    const [isError, setisError] = useState(false);


    const getProducts = async () =>{
        try{
            let respons = await axios.get('https://api-tdp-2022.vercel.app/api/categories');
            setProducts(respons.data.data);
            setisLoading(false);
            }
        catch(e){
            setisError(true);
        }
    }

    useEffect(()=>{
        setisLoading(true);
        getProducts();
    }, [])

  return (
    <div>
        <div className="content padding" style={{ maxWidth: '1564px' }}>
            <div className="container padding-32" id="projects">
                <h3 className="border-bottom border-light-grey padding-16">Products Category</h3>
            </div>
            <div className="row-padding">
            {
                !isLoading ? 
                products.map((product, index) => {
                return(
                    <div className="col l3 m6 margin-bottom" key={index}>
                        <a href={`/shop?category=${product.id}`}>
                            <div className="display-container" style={{ boxShadow: '0 2px 7px #dfdfdf', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '300px', padding: '80px' }}>
                                <div className="display-topleft black padding">{product?.name}</div>
                                <img src={product?.image} alt="House" style={{ maxWidth: '100%', minHeight: '100%' }} />
                            </div>
                        </a>
                    </div>
                )
                }): 
                isLoading ? 
                <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
                    Loading . . .
                </div>
                : 
                isError ?
                <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
                    Error . . .
                </div>
                :''
            }
            </div>

            <Map/>
        </div>
    </div>
  )
}

export default ProductCategory